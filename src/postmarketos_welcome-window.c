/* postmarketos_welcome-window.c
 *
 * Copyright 2020 Martijn Braam
 * Copyright 2022 knuxify, Luca Weiss, Oliver Smith
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "postmarketos_welcome-config.h"
#include "postmarketos_welcome-window.h"

#define OS_RELEASE_LOCATION "/etc/os-release"
#define OS_RELEASE_VERSION "VERSION"

struct _PostmarketosWelcomeWindow
{
  AdwApplicationWindow  parent_instance;

  /* Template widgets */
  AdwHeaderBar        *header_bar;
  GtkButton           *exit_button;
  GtkButton           *previous_button;
  GtkButton           *next_button;
  AdwCarousel         *carousel;
  GtkButton           *close_button;
  GtkWidget           *welcome_title;

  /* Phosh specific */
  GtkWidget           *phosh_button;
  GtkWidget           *phosh_header;
  GtkWidget           *phosh_icon;
  GtkWidget           *phosh_text;
  GtkWidget           *phosh_welcome_new_image;
  GtkWidget           *phosh_welcome_new_text;

  /* GNOME Shell on mobile specific */
  GtkWidget           *gnome_header;
  GtkWidget           *gnome_icon;
  GtkWidget           *gnome_text;
};

G_DEFINE_TYPE (PostmarketosWelcomeWindow, postmarketos_welcome_window, ADW_TYPE_APPLICATION_WINDOW)

static void
postmarketos_welcome_window_on_carousel_position_change (AdwCarousel *carousel, __attribute__((unused)) gdouble *pos, PostmarketosWelcomeWindow *win)
{
  gdouble _pos = adw_carousel_get_position(carousel);
  gint _n_pages = adw_carousel_get_n_pages(carousel);

  if (_pos < 1) {
    gtk_widget_set_opacity(GTK_WIDGET (win->previous_button), _pos);
    gtk_widget_set_can_target(GTK_WIDGET (win->previous_button), FALSE);
    gtk_widget_set_can_focus(GTK_WIDGET (win->previous_button), FALSE);

    gtk_widget_set_opacity(GTK_WIDGET (win->exit_button), (1 - _pos));
    gtk_widget_set_can_target(GTK_WIDGET (win->exit_button), TRUE);
    gtk_widget_set_can_focus(GTK_WIDGET (win->exit_button), TRUE);
  } else {
    gtk_widget_set_opacity(GTK_WIDGET (win->previous_button), 1);
    gtk_widget_set_can_target(GTK_WIDGET (win->previous_button), TRUE);
    gtk_widget_set_can_focus(GTK_WIDGET (win->previous_button), TRUE);

    gtk_widget_set_opacity(GTK_WIDGET (win->exit_button), 0);
    gtk_widget_set_can_target(GTK_WIDGET (win->exit_button), FALSE);
    gtk_widget_set_can_focus(GTK_WIDGET (win->exit_button), FALSE);
  }

  if (_pos > _n_pages - 2) {
    gtk_widget_set_opacity(GTK_WIDGET (win->next_button), (_n_pages - 1 - _pos));
    gtk_widget_set_can_target(GTK_WIDGET (win->next_button), FALSE);
    gtk_widget_set_can_focus(GTK_WIDGET (win->next_button), FALSE);
  } else {
    gtk_widget_set_opacity(GTK_WIDGET (win->next_button), 1);
    gtk_widget_set_can_target(GTK_WIDGET (win->next_button), TRUE);
    gtk_widget_set_can_focus(GTK_WIDGET (win->next_button), TRUE);
  }
}

static void
postmarketos_welcome_window_next_page (__attribute__((unused)) GtkButton *button, PostmarketosWelcomeWindow *win)
{
  AdwCarousel *carousel = win->carousel;
  guint current_page = adw_carousel_get_position(ADW_CAROUSEL (carousel));

  if (current_page != adw_carousel_get_n_pages(carousel) - 1) {
    adw_carousel_scroll_to(carousel, adw_carousel_get_nth_page(carousel, current_page + 1), TRUE);
  }
}

static void
postmarketos_welcome_window_previous_page (__attribute__((unused)) GtkButton *button, PostmarketosWelcomeWindow *win)
{
  AdwCarousel *carousel = win->carousel;
  guint current_page = adw_carousel_get_position(ADW_CAROUSEL (carousel));
  if (current_page != 0) {
    adw_carousel_scroll_to(carousel, adw_carousel_get_nth_page(carousel, current_page - 1), TRUE);
  }
}

static void
postmarketos_welcome_window_on_close_button (GtkButton *self)
{
  gtk_window_close (GTK_WINDOW (gtk_widget_get_native(GTK_WIDGET (self))));
}

static void
postmarketos_welcome_window_on_ssh_button (__attribute__ ((unused)) GtkButton *self)
{
  g_app_info_launch_default_for_uri("https://wiki.postmarketos.org/wiki/SSH", NULL, NULL);
}

static void
postmarketos_welcome_window_on_phosh_button (__attribute__ ((unused)) GtkButton *self)
{
  g_app_info_launch_default_for_uri("https://wiki.postmarketos.org/wiki/Phosh", NULL, NULL);
}

static void
postmarketos_welcome_window_on_chat_button (__attribute__ ((unused)) GtkButton *self)
{
  g_app_info_launch_default_for_uri("https://wiki.postmarketos.org/wiki/Matrix_and_IRC", NULL, NULL);
}

static void
postmarketos_welcome_window_on_issue_button (__attribute__ ((unused)) GtkButton *self)
{
  g_app_info_launch_default_for_uri("https://wiki.postmarketos.org/wiki/How_to_report_issues", NULL, NULL);
}

static const gchar *
get_os_release_field (const gchar *key)
{
  char *token;
  char *search = "=";
  FILE *file = fopen (OS_RELEASE_LOCATION, "r");
  if (!file) {
    fprintf(stderr, "could not open %s: %s\n", OS_RELEASE_LOCATION, strerror(errno));
    return NULL;
  }
  char line[128];
  while (fgets(line, sizeof (line), file) != NULL) {
    // Remove trailing newline
    line[strcspn(line, "\n")] = '\0';
    // Token will point to the part before the =
    token = strtok(line, search);
    g_debug ("token-a: %s\n", token);
    // Skip all unused lines
    if (!strcmp(token, key) == 0)
      continue;
    // Token will point to the part after the =
    token = strtok(NULL, search);
    g_debug ("token-b: %s\n", token);
    // Remove possible quotes
    if (token[0] == '"') {
      token++;
      int len = strlen(token);
      // Remove trailing quote
      if (token[len-1] == '"') {
        token[len-1] = '\0';
      }
    }

    fclose (file);

    GString *value = g_string_new (token);
    return g_string_free (value, FALSE);
  }
  fclose (file);
  fprintf (stderr, "could not find key %s in os-release\n", key);
  return NULL;
}

static const char *
get_session()
{
  const char *ret = getenv("XDG_SESSION_DESKTOP");

  if (!ret) {
    fprintf(stderr, "error: XDG_SESSION_DESKTOP is not set!\n");
    return "unknown";
  }

  return ret;
}

static void
postmarketos_welcome_window_class_init (PostmarketosWelcomeWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/postmarketos/Welcome/postmarketos_welcome-window.ui");
  gtk_widget_class_bind_template_child (widget_class, PostmarketosWelcomeWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, PostmarketosWelcomeWindow, exit_button);
  gtk_widget_class_bind_template_child (widget_class, PostmarketosWelcomeWindow, previous_button);
  gtk_widget_class_bind_template_child (widget_class, PostmarketosWelcomeWindow, next_button);
  gtk_widget_class_bind_template_child (widget_class, PostmarketosWelcomeWindow, carousel);
  gtk_widget_class_bind_template_child (widget_class, PostmarketosWelcomeWindow, close_button);
  gtk_widget_class_bind_template_child (widget_class, PostmarketosWelcomeWindow, welcome_title);

  /* Phosh specific */
  gtk_widget_class_bind_template_child (widget_class, PostmarketosWelcomeWindow, phosh_button);
  gtk_widget_class_bind_template_child (widget_class, PostmarketosWelcomeWindow, phosh_header);
  gtk_widget_class_bind_template_child (widget_class, PostmarketosWelcomeWindow, phosh_icon);
  gtk_widget_class_bind_template_child (widget_class, PostmarketosWelcomeWindow, phosh_text);
  gtk_widget_class_bind_template_child (widget_class, PostmarketosWelcomeWindow, phosh_welcome_new_image);
  gtk_widget_class_bind_template_child (widget_class, PostmarketosWelcomeWindow, phosh_welcome_new_text);

  /* GNOME Shell on mobile specific */
  gtk_widget_class_bind_template_child (widget_class, PostmarketosWelcomeWindow, gnome_header);
  gtk_widget_class_bind_template_child (widget_class, PostmarketosWelcomeWindow, gnome_icon);
  gtk_widget_class_bind_template_child (widget_class, PostmarketosWelcomeWindow, gnome_text);

  gtk_widget_class_bind_template_callback (widget_class, postmarketos_welcome_window_on_carousel_position_change);
  gtk_widget_class_bind_template_callback (widget_class, postmarketos_welcome_window_next_page);
  gtk_widget_class_bind_template_callback (widget_class, postmarketos_welcome_window_previous_page);
  gtk_widget_class_bind_template_callback (widget_class, postmarketos_welcome_window_on_close_button);
  gtk_widget_class_bind_template_callback (widget_class, postmarketos_welcome_window_on_ssh_button);
  gtk_widget_class_bind_template_callback (widget_class, postmarketos_welcome_window_on_phosh_button);
  gtk_widget_class_bind_template_callback (widget_class, postmarketos_welcome_window_on_chat_button);
  gtk_widget_class_bind_template_callback (widget_class, postmarketos_welcome_window_on_issue_button);
}

static void
postmarketos_welcome_window_init (PostmarketosWelcomeWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  /* Load application stylesheet */
  GdkDisplay *display = gtk_widget_get_display(GTK_WIDGET (self));
  GtkCssProvider *provider = gtk_css_provider_new();
  gtk_css_provider_load_from_resource(provider, "/org/postmarketos/Welcome/style.css");
  gtk_style_context_add_provider_for_display(display, GTK_STYLE_PROVIDER(provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

  /* Put pmOS version into welcome string */
  const gchar *os_version = get_os_release_field (OS_RELEASE_VERSION);
  if (!os_version) {
    fprintf(stderr, "failed to get os version, using default.\n");
    os_version = "edge";
  }
  g_print ("version from os-release: %s\n", os_version);
  GString *welcome_str = g_string_new (NULL);
  g_string_printf (welcome_str, "Welcome to postmarketOS %s", os_version);
  gtk_label_set_label (GTK_LABEL (self->welcome_title), g_string_free(welcome_str, FALSE));

  /* Hide UI specific widgets unless that UI is currently running */
  const char *session = get_session();
  if (strcmp(session, "phosh") != 0) {
    gtk_widget_set_visible (self->phosh_button, false);
    gtk_widget_set_visible (self->phosh_header, false);
    gtk_widget_set_visible (self->phosh_icon, false);
    gtk_widget_set_visible (self->phosh_text, false);
    gtk_widget_set_visible (self->phosh_welcome_new_image, false);
    gtk_widget_set_visible (self->phosh_welcome_new_text, false);
  } else if (strcmp(session, "gnome") != 0) {
    gtk_widget_set_visible (self->gnome_header, false);
    gtk_widget_set_visible (self->gnome_icon, false);
    gtk_widget_set_visible (self->gnome_text, false);
  }
}
